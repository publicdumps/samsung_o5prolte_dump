#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/platform/13540000.dwmmc0/by-name/RECOVERY:12648464:733b414af5877290de1dbbfb4f21d892823c63d2; then
  applypatch --bonus /system/etc/recovery-resource.dat \
          --patch /system/recovery-from-boot.p \
          --source EMMC:/dev/block/platform/13540000.dwmmc0/by-name/BOOT:6557712:0457027df638108f1f259dc29831f32c1d029e96 \
          --target EMMC:/dev/block/platform/13540000.dwmmc0/by-name/RECOVERY:12648464:733b414af5877290de1dbbfb4f21d892823c63d2 && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
